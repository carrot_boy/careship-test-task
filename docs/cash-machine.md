# Cash Machine - Withdraw

**URL** : `/v1/cash-machine/withdraw`

**Method** : `POST`

**Data constraints** Amount should be integer or float types.

## Request (example)

```json
{
    "amount": 50.00
}
```

## Response (example)

**Code** : `200 OK`

**Content**:


```json
{
    "notes": [20, 20, 10]
}
```

[Back](readme.md)