## Endpoints 
### Cash Machine - Withdraw

**URL** : `/v1/cash-machine/withdraw`

**Method** : `POST`

**Data constraints** Amount should be integer or float types.

```json
{
    "amount": 100.00
}
```

#### Success Responses

**Code** : `200 OK`

**Content** (example):

Dates have the following format `YYYY-MM-DD hh:mm:ss` and the size is shown in bytes

```json
{
    "id": 546,
    "name": "test_file_2.txt"
}
```



## Errors
When errors occurs the server responds with appropriate HTTP-code and payload. 
All payload have the same format.

For example:
```json
{
    "error_code": 404,
    "error_message": "Page not found"
}
```

[Back](readme.md)