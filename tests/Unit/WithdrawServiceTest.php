<?php

namespace Tests\Unit;

use App\CashMachine\WithdrawService;
use App\Exception\InvalidArgumentException;
use App\Exception\NoteUnavailableException;

class WithdrawServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var WithdrawService */
    protected $service;

    public function setUp()
    {
        $this->service = new WithdrawService;
    }

    public function dataException()
    {
        return [
            // Negative amount
            [-10, [], InvalidArgumentException::class],
            // Unavailable note
            [15, [], NoteUnavailableException::class],
        ];
    }

    public function dataGetNotes()
    {
        return [
            [0, []],
            [10, [10]],
            [20, [20]],
            [50, [50]],
            [100, [100]],
            [30, [20, 10]],
            [40, [20, 20]],
            [60, [50, 10]],
            [70, [50, 20]],
            [80, [50, 20, 10]],
            [90, [50, 20, 20]],
            [110, [100, 10]],
            [120, [100, 20]],
            [130, [100, 20, 10]],
            [170, [100, 50, 20]],
            [220, [100, 100, 20]],
            [360, [100, 100, 100, 50, 10]],
        ];
    }

    /**
     * @dataProvider dataGetNotes
     * @covers WithdrawService::<public>
     * @covers WithdrawService::divideAmountIntoNotes()
     * @covers WithdrawService::getNotes()
     * @covers WithdrawService::getAvailableNotes()
     *
     * @param int|float $inAmount
     * @param array $expectedStack
     *
     * @throws InvalidArgumentException
     * @throws NoteUnavailableException
     */
    public function testGetNotesSimpleCases($inAmount, $expectedStack)
    {
        $stack = $this->service->getNotes($inAmount);
        $this->assertEquals($expectedStack, $stack);
    }

    /**
     * @dataProvider dataException
     * @covers WithdrawService::isAmountNegative()
     * @covers WithdrawService::isNotesUnavailable()
     *
     * @param int|float $inAmount
     * @param array $expectedStack
     * @param null|string $expectedException
     *
     * @throws InvalidArgumentException
     * @throws NoteUnavailableException
     */
    public function testGetNotesWithException($inAmount, $expectedStack, $expectedException)
    {
        $this->expectException($expectedException);

        $stack = $this->service->getNotes($inAmount);
        $this->assertEquals($expectedStack, $stack);
    }
}