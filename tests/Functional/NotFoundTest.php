<?php

namespace Tests\Functional;

class NotFoundTest extends BaseTestCase
{
    public function testPageNotFound()
    {
        $response = $this->runApp('GET', '/non-existent-page');
        $actualResponseBody = (string) $response->getBody();
        $expectedResponse = json_encode([
            "error_code" => 404,
            "error_message" => 'Page not found',
        ]);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals($expectedResponse, $actualResponseBody);
    }
}