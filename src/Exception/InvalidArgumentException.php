<?php

namespace App\Exception;

class InvalidArgumentException extends \Exception
{
    const NEGATIVE_NUMBER_CODE = 2001;
}