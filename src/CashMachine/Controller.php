<?php

namespace App\CashMachine;

use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Exception\InvalidArgumentException;
use App\Exception\NoteUnavailableException;

class Controller
{
    /** @var WithdrawService */
    protected $service;

    /**
     * @param ContainerInterface $container
     */
    public function __construct($container)
    {
        $this->service = $container->get(WithdrawService::class);
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return Response
     * @throws InvalidArgumentException
     * @throws NoteUnavailableException
     */
    public function withdraw($request, $response)
    {
        $amount = $request->getAttribute('amount');
        $responseBody = [
            'notes' => $this->service->getNotes($amount),
        ];

        return $response->withJson($responseBody, 200);
    }
}