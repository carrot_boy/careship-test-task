<?php

namespace App\CashMachine;

use App\Exception\InvalidArgumentException;
use App\Exception\NoteUnavailableException;

class WithdrawService
{
    protected $availableNotes = [100, 50, 20, 10];

    /**
     * @param int|float $amount
     * @return array
     * @throws InvalidArgumentException
     * @throws NoteUnavailableException
     */
    public function getNotes($amount)
    {
        if ($this->isAmountNegative($amount)) {
            throw new InvalidArgumentException(
                "Amount should be positive",
                InvalidArgumentException::NEGATIVE_NUMBER_CODE
            );
        }

        if ($this->isNotesUnavailable($amount)) {
            throw new NoteUnavailableException(
                "Requested notes unavailable",
                NoteUnavailableException::UNAVAILABLE_NOTE_CODE
            );
        }

        return $this->divideAmountIntoNotes($amount);
    }

    /**
     * @param $amount
     * @return bool
     */
    protected function isAmountNegative($amount)
    {
        return $amount < 0;
    }

    /**
     * @param $amount
     * @return bool
     */
    protected function isNotesUnavailable($amount)
    {
        $smallestNote = end($this->availableNotes);
        reset($this->availableNotes);

        return ($amount % $smallestNote !== 0);
    }

    /**
     * @param $amount
     * @return array
     */
    protected function divideAmountIntoNotes($amount) {
        $remainder = $amount;
        $notes = [];
        foreach ($this->availableNotes as $note) {
            $available = $this->getAvailableNotes($remainder, $note);

            $notes = array_merge($notes, $available['stack']);
            $remainder = $available['remainder'];

            // early escape
            if (0 == $remainder) {
                break;
            }
        }

        return $notes;
    }

    /**
     * @param $amount
     * @param $note
     *
     * @return array
     */
    protected function getAvailableNotes ($amount, $note) {
        $numOfNotes = floor($amount / $note);

        return [
            'stack' => array_fill(0, $numOfNotes, $note),
            'remainder' => ($amount % $note),
        ];
    }
}