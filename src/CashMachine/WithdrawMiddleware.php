<?php

namespace App\CashMachine;

use App\Exception\WrongRequestException;
use Slim\Http\Request;
use Slim\Http\Response;

class WithdrawMiddleware
{
    /**
     * @param  Request $request
     * @param  Response $response
     * @param  callable $next
     *
     * @return mixed
     * @throws WrongRequestException
     */
    public function __invoke($request, $response, $next)
    {
        $amount = $request->getParsedBodyParam('amount');

        if (false === isset($amount)) {
            $message = sprintf(WrongRequestException::MESSAGE_MISSING_REQUIRED_PARAMETER, 'amount');
            throw new WrongRequestException($message, WrongRequestException::CODE_MISSING_REQUIRED_PARAMETER);
        }

        if (is_int($amount) || is_float($amount)) {
            $requestWithAttribute = $request->withAttribute('amount', $amount);

            return $next($requestWithAttribute, $response);
        }

        $message = sprintf(WrongRequestException::MESSAGE_INCORRECT_FORMAT, 'amount');
        throw new WrongRequestException($message, WrongRequestException::CODE_INCORRECT_FORMAT);
    }
}