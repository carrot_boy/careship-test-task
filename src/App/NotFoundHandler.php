<?php

namespace App\Common;

use Monolog\Logger;
use Slim\Http\Request;
use Slim\Http\Response;


class NotFoundHandler
{
    /** @var Logger */
    protected $logger;

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param  Request $request
     * @param  Response $response
     *
     * @return Response
     */
    public function __invoke($request, $response)
    {
        $message = sprintf("Requested page '%s' not found", $request->getUri());
        $this->logger->info($message);
        $responseBody = [
            "error_code" => 404,
            "error_message" => 'Page not found',
        ];

        return $response->withJson($responseBody, 404);
    }

}