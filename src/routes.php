<?php

use App\CashMachine\WithdrawMiddleware;
use App\CashMachine\Controller as WithdrawController;
use Slim\HttpCache\Cache;

// Cache all requests (one day)
$app->add(new Cache('public', 86400));

$app->group('/v1/cash-machine', function () {
    $this->post('/withdraw', WithdrawController::class . ':withdraw')
        ->add(new WithdrawMiddleware);
});